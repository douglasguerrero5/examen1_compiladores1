#include "expr_parser.h"
#include <iostream>

using namespace std;

int Parser::parse() {
    exprCount = 0;
    token = lexer.getNextToken();
    Stmt();
    return exprCount;
}

void Parser::Stmt(){
    Expr();
    StmtP();
}

void Parser::StmtP(){
    if(token == Token::Semicolon){
        token = lexer.getNextToken();
        Expr();
        StmtP();
    } 
    exprCount++;
}

void Parser::Expr(){
    Term();
    ExprP();
}

void Parser::ExprP(){
    if(token == Token::OpAdd || token == Token::OpSub){
        token = lexer.getNextToken();
        Term();
        ExprP();
    } 
}

void Parser::Term(){
    Factor();
    TermP();
}

void Parser::TermP(){
    if(token == Token::OpMul || token == Token::OpDiv){
        token = lexer.getNextToken();
        Term();
        ExprP();
    } 
}

void Parser::Factor(){
    if(token == Token::Number){
        token = lexer.getNextToken();
    }else if(token == Token::OpenPar){
        token = lexer.getNextToken();
        Expr();
        if(token != Token::ClosePar){
        }
        token = lexer.getNextToken();
    }else {
        throw "Error";
    }    
}
