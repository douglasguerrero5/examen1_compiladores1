#ifndef _PARSER_H
#define _PARSER_H

#include "expr_lexer.h"

class Parser {
public:
    Parser(Lexer& lexer): lexer(lexer) {}
    int parse();

private:
    Lexer& lexer;
    Token token;
    int exprCount;

private:
    void Stmt();
    void StmtP();
    void Expr();
    void ExprP();
    void Term();
    void TermP();
    void Factor();
};

#endif
