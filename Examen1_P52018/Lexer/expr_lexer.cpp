#include "expr_lexer.h"

void ExprLexer::ungetChar(){
    in.unget();
}

char ExprLexer::getNextChar(){
    return in.get();
}

Token ExprLexer::getNextToken() {
    text="";
    while(1){
        c = getNextChar();
        if(c == ' ' || c == '\t' || c == '\n' ){
            continue;
        }else if(c == '#'){
            c = getNextChar();
            if(c == '!'){
                c = getNextChar();
                bool endOfBlock = false;
                while(!endOfBlock){
                    c = getNextChar();
                    if(c == '!'){
                        c = getNextChar();
                        if(c == '#'){
                            endOfBlock = true;
                            c = getNextChar();
                        }
                    }
                }
                ungetChar();
            }else {
                c = getNextChar();
                while(c != '\n' && c != EOF){
                    c = getNextChar();
                }
                ungetChar();
            }
        }else if(c == '('){
            text += c;
            return Token::OpenPar;
        }else if(c == ')'){
            text += c;
            return Token::ClosePar;
        }else if(c == ';'){
            text += c;
            return Token::Semicolon;
        }else if(c == '+'){
            text += c;
            return Token::OpAdd;
        }else if(c == '-'){
            text += c;
            return Token::OpSub;
        }else if(c == '*'){
            text += c;
            return Token::OpMul;
        }else if(isdigit(c)){
            text += c;
            while(isdigit(c) || c == '.'){
                c = getNextChar();
                text += c;
            }
            text = text.substr(0, text.size()-1);
            ungetChar();
            return Token::Num;
        }else if(isalpha(c) || c == '_'){
            text += c;
            while(isalnum(c) || c == '_'){
                c = getNextChar();
                text += c;
            }
            ungetChar();
            return Token::Id;
        }else if(c == EOF){
            return Token::Eof;
        }
    }
    return Token::Error;
}